import PropTypes from 'prop-types';

export default function AppTitle({ title }) {
  return <h1 id="pageTitle">{title}</h1>;
}

AppTitle.propTypes = {
  title: PropTypes.string.isRequired,
};
